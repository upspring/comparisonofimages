package com.github.highlight.controller;

/**
 * Created by sindh on 3/28/2017.
 */

//import com.beust.jcommander.internal.Lists;

import com.github.kilianB.hashAlgorithms.AverageHash;
import com.github.kilianB.hashAlgorithms.HashingAlgorithm;
import com.github.kilianB.hash.Hash;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;


import org.springframework.web.multipart.commons.CommonsMultipartFile;

import static java.lang.StrictMath.round;


@Controller

public class HomeController extends Thread {


    private int keyLength = 64;
    private HashingAlgorithm hasher = new AverageHash(keyLength);
    private HashMap<String, BufferedImage> images = new HashMap<String,BufferedImage>();



    @RequestMapping(value="/",method = RequestMethod.GET)
    public  String compare(HttpSession session,HttpServletRequest request)
    {

        return "image";
    }

    @RequestMapping(value="/compare",method = RequestMethod.POST)
    public  void compareee(HttpServletRequest request, HttpServletResponse response,@RequestParam CommonsMultipartFile file,HttpSession session,@RequestParam CommonsMultipartFile alterFile) throws  IOException
    {

        String path=session.getServletContext().getRealPath("/");

        String filename=file.getOriginalFilename();
        String image2=alterFile.getOriginalFilename();

        System.out.println("file name--->"+filename);
        System.out.println(path);
        System.out.println("check"+path+"resources\\"+filename);
        //System.out.println(path+" "+image2);
        file.transferTo(new File(path+"resources\\"+filename));
        alterFile.transferTo(new File(path+"resources\\"+image2));

        CompareImages(filename,image2,request);
    }



    @RequestMapping(value="/comparisonOfImage",method = RequestMethod.POST)
    public @ResponseBody  void comparisonOfImage(HttpServletRequest request, @RequestParam CommonsMultipartFile file,HttpSession session) throws  IOException
    {
        String path=session.getServletContext().getRealPath("/");

        String filename=file.getOriginalFilename();
        //String image2=alterFile.getOriginalFilename();

        System.out.println("file name--->"+filename);
        System.out.println(path);
        System.out.println("check"+path+"resources/"+filename);
        //System.out.println(path+" "+image2);
        file.transferTo(new File(path+"resources/"+filename));
        //alterFile.transferTo(new File(path+"resources\\"+image2));

        //CompareImages(filename,image2,path,request);


    }



    @RequestMapping(value="/comparisonOfImage1",method = RequestMethod.POST)
    public @ResponseBody void comparisonOfImage1 (HttpServletRequest request, @RequestParam CommonsMultipartFile file,HttpSession session) throws  IOException
    {
        String path=session.getServletContext().getRealPath("/");

        String filename=file.getOriginalFilename();
        //String image2=alterFile.getOriginalFilename();

        System.out.println(path+"resources/"+filename);
        //System.out.println(path+" "+image2);
        file.transferTo(new File(path+"resources/"+filename));
        //alterFile.transferTo(new File(path+"resources\\"+image2));

        //CompareImages(filename,image2,path,request);


    }

    public  void compareImages(String file,String alterFile,HttpServletRequest request)
    {

        System.out.println("test compare"+file+alterFile);
        loadImages(file,alterFile,request);


        String file1=file;
        String alterFile1=alterFile;
        HttpSession session =request.getSession();

        if (file1.indexOf(".") > 0)
            file1 = file1.substring(0, file1.lastIndexOf("."));


        if (alterFile1.indexOf(".") > 0)
            alterFile1 = alterFile1.substring(0, alterFile1.lastIndexOf("."));

        boolean t=compareTwoImages(images.get(file1),images.get(alterFile1));
        System.out.println("t----->"+t);

    }

    @RequestMapping(value="/comparisonOfImage",method = RequestMethod.GET)
    public @ResponseBody boolean CompareImages(@RequestParam(value ="file" ) String file ,@RequestParam(value="alterFile") String alterFile,HttpServletRequest request) {

        System.out.println("test"+file+alterFile);
        loadImages(file,alterFile,request);


        String file1=file;
        String alterFile1=alterFile;
        HttpSession session =request.getSession();

        if (file1.indexOf(".") > 0)
            file1 = file1.substring(0, file1.lastIndexOf("."));


        if (alterFile1.indexOf(".") > 0)
            alterFile1 = alterFile1.substring(0, alterFile1.lastIndexOf("."));

        boolean t=compareTwoImages(images.get(file1),images.get(alterFile1));
        System.out.println("t----->"+t);

        return  t;
        // Compare each picture to each other
        /*images.forEach((imageName, image) -> {

            images.forEach((imageName2, image2) -> {

                formatOutput(imageName, imageName2, compareTwoImages(image, image2));

            });
        });*/
    }

    /**
     * Compares the similarity of two images.
     * @param image1	First image to be matched against 2nd image
     * @param image2	The second image
     * @return	true if the algorithm defines the images to be similar.
     */
    public boolean compareTwoImages(BufferedImage image1, BufferedImage image2) {

        //Generate the hash for each image
        Hash hash1 = hasher.hash(image1);
        Hash hash2 = hasher.hash(image2);

        //Compute a similarity score
        // Ranges between 0 - 1. The lower the more similar the images are.
        double similarityScore = hash1.normalizedHammingDistance(hash2);

        return similarityScore < 0.4d;
    }

    private void formatOutput(String image1, String image2, boolean similar) {
        String format = "| %-11s | %-11s | %-8b |%n";
        System.out.printf(format, image1, image2, similar);
    }

    private void loadImages(String file ,String alterFile,HttpServletRequest request) {
        // Load images
        try {

                String file1=file;
                String alterFile1=alterFile;


            HttpSession session =request.getSession();
            System.out.println(session.getServletContext().getResource("resources"));
            if (file1.indexOf(".") > 0)
                file1 = file1.substring(0, file1.lastIndexOf("."));


            if (alterFile1.indexOf(".") > 0)
                alterFile1 = alterFile1.substring(0, alterFile1.lastIndexOf("."));
            System.out.println("filr--->"+file+alterFile);

            images.put(file1, ImageIO.read(session.getServletContext().getResourceAsStream("resources/"+file)));
            images.put(alterFile1, ImageIO.read(session.getServletContext().getResourceAsStream("resources/"+alterFile)));
            //images.put("copyright", ImageIO.read(getClass().getResourceAsStream("images/copyright.jpg")));
            //images.put("highQuality", ImageIO.read(getClass().getResourceAsStream("images/highQuality.jpg")));
            //images.put("lowQuality", ImageIO.read(getClass().getResourceAsStream("images/lowQuality.jpg")));
            //images.put("thumbnail", ImageIO.read(getClass().getResourceAsStream("images/thumbnail.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Print header
        System.out.println("|   Image 1   |   Image 2   | Similar  |");
    }
}

