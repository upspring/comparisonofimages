<%--
  Created by IntelliJ IDEA.
  User: Upspring
  Date: 15-05-2021
  Time: 12:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>Image Comparison</title>
    <link href="<%=request.getContextPath()%>/resources/css/style.css" rel="stylesheet">
    <link href="<%=request.getContextPath()%>/resources/css/upload.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
 <!--<form action="<%=request.getContextPath()%>/compare" method="post" enctype="multipart/form-data">
    Select File: <input type="file" name="file"/>
    Select File: <input type="file" name="alterFile"/>
    <input type="submit" value="Upload File"/>
</form>

-->



<div class="row">

    <div class="col-md-4 upload-box-container">

        <div class="card card-hover d-flex justify-content-right no-block mr-1">
            <div class="card-header">
                <span>Select Image and Compare</span>
                <div class="circle-close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                </div>
            </div>

            <div class="img-resize">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M4 14h6v6M3 21l6.1-6.1M20 10h-6V4M21 3l-6.1 6.1"></path></svg>
                <span onclick="window.open('http://www.simpleimageresizer.com/upload','_blank')">Resize Image</span>
            </div>
            <div class="row" >
                <div class="col-md-6">
                    <div class="img-box">
                        <div class="img-box-header" id="img-box-header-l">
                            <img src="../../assets/images/users/1.jpg" style="display:none;">
                            <div class="img-content-holder" id="img-content-holder-l">
                                <div class="circle">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#888" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M3 15v4c0 1.1.9 2 2 2h14a2 2 0 0 0 2-2v-4M17 8l-5-5-5 5M12 4.2v10.3"/></svg>
                                </div>
                            </div>
                            <span class="browse-select" style="margin-left: 10px;">Browse to select Image</span>
                        </div>
                        <div class="img-box-footer" id="img-box-footer-l">
                            <span>Select Image</span>
                        </div>
                    </div>
                    <form id="image1" name="image1"   enctype="multipart/form-data">
                        <div class="fileContainer sprite">
                            <span>Browse</span>
                            <input type="file"  value="Browse" name="file" id="input-left" accept="image/*">
                        </div>
                    </form>
                </div>
                <div class="col-md-6 col-md-pull-1">
                    <div class="img-box">
                        <div class="img-box-header" id="img-box-header-r">
                            <img src="../../assets/images/users/nature.jpeg" style="display:none">
                            <div class="img-content-holder" id="img-content-holder-r">
                                <div class="circle">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#888" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M3 15v4c0 1.1.9 2 2 2h14a2 2 0 0 0 2-2v-4M17 8l-5-5-5 5M12 4.2v10.3"/></svg>
                                </div>

                            </div>
                            <span class="browse-select" style="margin-left: 10px;">Browse to select Image</span>
                        </div>
                        <div class="img-box-footer" id="img-box-footer-r">
                            <span>Select Image</span>
                        </div>
                    </div>
                    <form  id="image2"  name="image2"   enctype="multipart/form-data">
                        <div class="fileContainer sprite">
                            <span>Browse</span>
                            <input type="file"  name="file" value="Browse" id="input-right" accept="image/*">
                        </div>
                    </form>
                </div>
            </div>

            <div class="img-info">
                <span class="img-info-text" id="result"></span>
            </div>

            <div class="card-footer">
                <input type="button" value="Compare"  id="submitForms" >
            </div>

        </div>

    </div>
</div>

<script>
    var imageWidth=0,imageHeight=0,alterImageWidth=0,alterImageHeight=0;
    var imgStatus="";
   document.getElementById("submitForms").addEventListener("click",function (e)
   {

       document.getElementById("submitForms").disabled=true;

       e.preventDefault();



       var file = $("#input-left").val().split('\\').pop();
       console.log(file);
       var alterFile = $("#input-right").val().split('\\').pop();
       console.log(alterFile)
       if(file==="" || alterFile ==="")
       {
           document.getElementById("result").innerHTML="Please select an image";
           document.getElementById("submitForms").disabled=false;
           return;
       }

       if(file===alterFile)
       {
           //alert("Use different image name");
           document.getElementById("result").innerHTML="Use Different Image name";
           document.getElementById("submitForms").disabled=false;
           return;

       }

       var suc=0;
       var suc1=0;
       var data = new FormData();
       input = document.getElementById('input-left');
       data.append('file', input.files[0]);
console.log(input.files[0]);



       console.log("file" + data.get("file"));


       $.ajax({
           method: 'POST',
           enctype: 'multipart/form-data',
           url: '/comparisonOfImage',
           data: data,
           processData: false,
           contentType: false,
           success: function (data) {

           },
           error: function (xhr, ajaxOptions, thrownError) {

           }
       });


       var data1 = new FormData();
       input1 = document.getElementById('input-right');
       data1.append('file', input1.files[0]);
       console.log( input1.files[0]);
       console.log(imageWidth,imageHeight,alterImageWidth,alterImageHeight)


       if(imageHeight===alterImageHeight && imageWidth===alterImageWidth)
       {
           console.log(" Both images are same resoultion");
           imgStatus=" Both images are same resoultion";
       }
       else if(imageHeight>alterImageHeight || imageWidth > alterImageWidth)
       {
           console.log("second image has low resolution")
           imgStatus="Second image has low resolution";
       }
       else
       {
           console.log("second image has high resolution")
           imgStatus="Second image has high resolution";
       }


       $.ajax({
           method: 'POST',
           enctype: 'multipart/form-data',
           url: '/comparisonOfImage1',
           data: data1,
           processData: false,
           contentType: false,
           success: function (data) {


                   $.ajax({
                       url: "/comparisonOfImage?file=" + file + "&alterFile=" + alterFile,
                       type: 'get',
                       success: function (result) {
                           // alert(result);
                           if(result===true) {

                               document.getElementById("result").innerHTML = file + " and " + alterFile + " are Same Image.\n"+imgStatus;
                           }
                           if(result===false)
                               document.getElementById("result").innerHTML=file+" and "+alterFile+" are Different Image";


                           document.getElementById("submitForms").disabled="false"
                       }
                   });

           },
           error: function (xhr, ajaxOptions, thrownError) {

           }
       });

       //document.getElementById("image1").submit();
       //document.getElementById("image2").submit();




   });



    /* left Side Image Select */
    $("#input-left").change(function (){
        document.getElementById("submitForms").disabled=false;
        var fileName = $(this).val().split('\\').pop();
        if(fileName.length >0){
            console.log('inside of file name path');
            $('#img-box-footer-l').children('span').html(fileName);
        }
        else{
            $('#img-box-footer-l').children('span').html("Choose file");
        }
    });
    //file input preview
    function readURLLeft(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                console.log('inside of img path');

                document.getElementById('img-content-holder-l').style="display:none";
                $('#img-content-holder-l').parent().children('span').hide();
                $('#img-content-holder-l').parent().children('img').css("display", "block");
                $('#img-box-header-l img').attr('src', e.target.result);

            }
            reader.readAsDataURL(input.files[0]);

            var img = new Image();

            img.src = window.URL.createObjectURL(input.files[0]);

            img.onload = function() {
                imageWidth = img.naturalWidth,
                    imageHeight = img.naturalHeight;
            };
        }
    }
    $("#input-left").change(function(){
        document.getElementById("submitForms").disabled=false;
        document.getElementById("result").innerHTML="";
        readURLLeft(this);
    });

    /* Right Side Image Select */
    $("#input-right").change(function (){
        var fileName = $(this).val().split('\\').pop();
        if(fileName.length >0){
            console.log('inside of file name path');
            $('#img-box-footer-r').children('span').html(fileName);
        }
        else{
            $('#img-box-footer-r').children('span').html("Choose file");
        }
    });
    //file input preview
    function readURLRight(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                console.log('inside of img path');
                document.getElementById('img-content-holder-r').style="display:none";
                $('#img-content-holder-r').parent().children('span').hide();
                $('#img-content-holder-r').parent().children('img').css("display", "block");
                $('#img-box-header-r img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);

            var img = new Image();

            img.src = window.URL.createObjectURL(input.files[0]);

            img.onload = function() {
                alterImageWidth = img.naturalWidth,
                    alterImageHeight = img.naturalHeight;
                console.log("width",imageWidth,imageHeight);
                window.URL.revokeObjectURL( img.src );

            };
        }
    }
    $("#input-right").change(function(){
        document.getElementById("submitForms").disabled=false;
        document.getElementById("result").innerHTML="";
        readURLRight(this);
    });
</script>
</body>
</html>
